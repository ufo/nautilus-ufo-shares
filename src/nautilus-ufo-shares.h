/*
 *  nautilus-ufo-shares.h
 * 
 *  Copyright (C) 2004, 2005 Free Software Foundation, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef NAUTILUS_UFO_SHARES_H
#define NAUTILUS_UFO_SHARES_H

#include <glib-object.h>

#include <libnautilus-extension/nautilus-menu-provider.h>

G_BEGIN_DECLS

/* Declarations for the UFO shares extension object.  This object will be
 * instantiated by nautilus.  It implements the GInterfaces 
 * exported by libnautilus. */


#define NAUTILUS_TYPE_UFO_SHARES   (nautilus_ufo_shares_get_type ())
#define NAUTILUS_UFO_SHARES(o)	   (G_TYPE_CHECK_INSTANCE_CAST ((o), NAUTILUS_TYPE_UFO_SHARES, NautilusUFOShares))
#define NAUTILUS_IS_UFO_SHARES(o)  (G_TYPE_CHECK_INSTANCE_TYPE ((o), NAUTILUS_TYPE_UFO_SHARES))

typedef struct _DialogCallbackThreadArgs DialogCallbackThreadArgs;
typedef struct _CallbackThreadArgs       CallbackThreadArgs;
typedef struct _NautilusUFOShares        NautilusUFOShares;
typedef struct _NautilusUFOSharesClass   NautilusUFOSharesClass;

typedef enum {
    MY_FILE,
    OTHER_FILE,
    NONE_FILE
} ShareFileInfo;

typedef enum {
    SHARING,
    TAGGING
} ShareFileJob;

struct _DialogCallbackThreadArgs {
	NautilusFileInfo *file_info;
	gchar *item_name;
	ShareFileJob job;
};

struct _CallbackThreadArgs {
	NautilusMenuItem *item;
	NautilusFileInfo *file_info;
};

struct _NautilusUFOShares {
	GObject parent_slot;
};

struct _NautilusUFOSharesClass {
	GObjectClass parent_slot;
};

GType nautilus_ufo_shares_get_type      (void);
void  nautilus_ufo_shares_register_type (GTypeModule *module);

G_END_DECLS

#endif
