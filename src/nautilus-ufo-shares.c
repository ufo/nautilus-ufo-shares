/*
 *  nautilus-ufo-shares.c
 * 
 *  Copyright (C) 2004, 2005 Free Software Foundation, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
 #include <config.h> /* for GETTEXT_PACKAGE */
#endif

#include "nautilus-ufo-shares.h"

#include <libnautilus-extension/nautilus-menu-provider.h>
#include <libnautilus-extension/nautilus-info-provider.h>
#include <libnautilus-extension/nautilus-extension-types.h>

#include <libnotify/notify.h>

#include <glib.h>
#include <glib/gi18n-lib.h>
#include <gio/gio.h>
#include <gtk/gtk.h>
#include <libgnome/gnome-desktop-item.h>

#include <stdio.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h> /* for atoi */
#include <string.h> /* for strcmp */
#include <unistd.h> /* for chdir */
#include <libgen.h> /* for basename, dirname */
#include <pwd.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <attr/xattr.h>
#include <syslog.h>

static void nautilus_ufo_shares_instance_init (NautilusUFOShares     *cvs);
static void nautilus_ufo_shares_class_init    (NautilusUFOSharesClass *class);

static GType ufo_shares_type = 0;

GList *tags = NULL;
GList *parts = NULL;
GList *parts_dirents = NULL;
GList *tags_dirents = NULL;
ShareFileInfo share_file_info = NONE_FILE;

gchar *cached_uri = NULL;

gchar *overlaypath = NULL;
gchar *userdirpath = NULL;
gchar *tagsviewpath = NULL;
gchar *sharesviewpath = NULL;


static ShareFileInfo
get_share_file_info (const char *uri)
{
    int size;
    char value[1];

    size = getxattr(uri, "tsumufs.is-owner", value, 1);
    if (size < 0)
       return NONE_FILE;

    if (atoi(value) == 1)
       return MY_FILE;

    return OTHER_FILE;
}

static void
disable_menu_item (NautilusMenuItem *item) {
    GValue value;

    memset(&value, 0, sizeof(value));
    g_value_init(&value, G_TYPE_BOOLEAN);

    g_value_set_boolean(&value, FALSE);
    g_object_set_property(G_OBJECT(item), "sensitive", &value);
}

static gint
ufo_shares_notify (char* title, char * string) {
    NotifyNotification *example;
    example = notify_notification_new(title, string, NULL);

    notify_notification_set_timeout(example, 6000);
    notify_notification_set_urgency (example, NOTIFY_URGENCY_NORMAL);
    notify_notification_show(example, NULL);

    return 0;
}

static gint
ufo_shares_notify_error (char * string) {
    gchar *message = _("An error occurred during the operation.");
    return ufo_shares_notify(_("Operation failed"),
                        g_strdup_printf("%s\n(%s).", message, string));
}

static void
invalidate_cached_uri () {
    if (tags != NULL) {
       g_list_foreach(tags, (GFunc)g_free, NULL);
       g_list_free(tags);
       tags = NULL;
    }
    if (tags_dirents != NULL) {
       g_list_foreach(tags_dirents, (GFunc)g_free, NULL);
       g_list_free(tags_dirents);
       tags_dirents = NULL;
    }
    if (parts != NULL) {
       g_list_foreach(parts, (GFunc)g_free, NULL);
       g_list_free(parts);
       parts = NULL;
    }
    if (parts_dirents != NULL) {
       g_list_foreach(parts_dirents, (GFunc)g_free, NULL);
       g_list_free(parts_dirents);
       parts_dirents = NULL;
    }

    g_free(cached_uri);
    cached_uri = NULL;

    share_file_info = NONE_FILE;
}

static GList*
coma_separated_to_list (GList *list, char *string) {
    gchar *str = strtok(string, ",");

    while (str != NULL) {
       list = g_list_append(list, g_strdup(str));
       str = strtok(NULL, ",");
    }
    return list;
}

static char*
extended_attribute_to_string (char* uri, char* xattr) {
    int size;
    char* result = NULL;

    if ((size=getxattr(uri, xattr, NULL, 0)) > 0) {
       result = (char*) malloc(size+1);
       size = getxattr(uri, xattr, result, size);
       result[size] = '\0';
    }
    return result;
}

static GList*
extended_attribute_to_list (GList *list, char* uri, char* xattr) {
    char* result = extended_attribute_to_string(uri, xattr);
    if (result != NULL) {
       list = coma_separated_to_list(list, result);
       g_free(result);
    }

    return list;
}

static GList*
item_list_from_dirents (GList *list, const char* dirpath, GList *excluded) {
    struct dirent *dit;
    DIR         *dip;

    /* Fill the list with dirpath dirents */
    if ((dip = opendir(dirpath)) != NULL) {
       while ((dit = readdir(dip)) != NULL) {

          if ((strcmp(dit->d_name, ".") == 0 || strcmp(dit->d_name, "..") == 0))
             continue;

          int ptr;
          for (ptr = 0 ; ptr < g_list_length (excluded) ; ptr++) {
             if (strcmp(dit->d_name, g_list_nth_data(excluded, ptr)) == 0) {
                ptr = -1;
                break;
             }
          }
          if (ptr == -1) continue;

          list = g_list_append(list, g_strdup((gpointer)dit->d_name));
       }

       closedir(dip);
    }
    return list;
}

static void
copy_to_sync_folder (NautilusFileInfo *file_info) {
    gchar* srcuri = g_filename_from_uri(nautilus_file_info_get_activation_uri (file_info), NULL, NULL);
    gchar* desturi = g_strdup_printf("%s/%s", userdirpath, basename(g_strdup(srcuri)));

    FILE *src;
    FILE *dest;
    int byte = 0;

    if ((src = fopen(srcuri, "rxb")) != NULL) {
       if ((dest = fopen(desturi, "wxb")) != NULL) {
          while (byte != EOF) {
             byte = fgetc(src);
             fputc(byte, dest);
          }
          fclose(dest);
       }
       fclose(src);
    }
    g_free (desturi);
}

static gboolean
common_callback (NautilusFileInfo *file_info, const char *item_name, ShareFileJob job, gboolean inverse) {
    gchar *dirpath = NULL;
    gchar *targetpath = NULL;
    gchar *viewpath = NULL;
    gchar *actionxattr = NULL;
    gchar *eexist = NULL, *eexisttitle = NULL;
    gchar *enoent = NULL, *enoenttitle = NULL;
    gchar *success = NULL, *successtitle = NULL;

    NautilusFileInfoIface *file_iface = NAUTILUS_FILE_INFO_GET_IFACE(file_info);
    file_iface->add_emblem(file_info, "loading");

    gchar *uri = g_filename_from_uri(nautilus_file_info_get_activation_uri (file_info), NULL, NULL);
    gchar *filename = basename(uri);

    switch (job) {
        case SHARING:
            viewpath = sharesviewpath;
            eexist = g_strdup_printf(_("The file '%s' is already shared with %s,\nplease select another friend."),
                                     filename, item_name);
            enoent = g_strdup_printf(_("The user '%s' does not exists,\nplease select another friend."),
                                     item_name);
            if (!inverse) {
                actionxattr = "tsumufs.myshares.share";

                success = g_strdup_printf(_("The file '%s' has been successfully shared with %s."),
                                          filename, item_name);
                successtitle = _("File successfully shared");
            } else {
                actionxattr = "tsumufs.myshares.unshare";

                success = g_strdup_printf(_("The share of the file '%s' with %s has been successfully canceled."),
                                          filename, item_name);
                successtitle = _("Share successfully canceled");
            }
            eexisttitle = _("File already shared");
            enoenttitle = _("User does not exists");
            break;

        case TAGGING:
            viewpath = tagsviewpath;
            eexist = g_strdup_printf(_("The file '%s' is already tagged with '%s',\nplease type another tag name."),
                                     filename, item_name);
            enoent = g_strdup_printf(_("The tag '%s' is invalid,\nplease select another friend."),
                                     item_name);
            if (!inverse) {
                actionxattr = "tsumufs.staredbytag.tag";

                success = g_strdup_printf(_("The file '%s' has been successfully tagged with %s."),
                                          filename, item_name);
                successtitle = _("File successfully tagged");
            } else {
                actionxattr = "tsumufs.staredbytag.untag";

                success = g_strdup_printf(_("The file '%s' has been successfully removed from tag %s."),
                                          filename, item_name);
                successtitle = _("File successfully removed from tag");
            }
            eexisttitle = _("File already tagged");
            enoenttitle = _("Invalid tag");
            break;
    }
    dirpath = g_strdup_printf ("%s/%s", viewpath, item_name);
    targetpath = g_strdup_printf ("%s/%s", dirpath, filename);

    gboolean result = TRUE;
    if (!inverse) {
        if (mkdir(dirpath, 0755) < 0 && errno != EEXIST) {
            if (errno == ENOENT)
                ufo_shares_notify(enoenttitle, enoent);
            else
                ufo_shares_notify_error(strerror(errno));
            result = FALSE;
        }
    }

    if (result == TRUE) {
        // Finally perform the action (share/unshare/tag/untag)
        if (setxattr(uri, actionxattr, item_name, strlen(item_name), 0) < 0) {
            if (errno == EEXIST)
                ufo_shares_notify(eexisttitle, eexist);
            else
                ufo_shares_notify_error(strerror(errno));
            result = FALSE;
        } else {
            ufo_shares_notify(successtitle, success);
        }
    }

    file_iface->remove_emblem(file_info, "loading");

    g_free (dirpath);
    g_free (targetpath);
    g_free (eexist);
    g_free (enoent);
    g_free (success);

    invalidate_cached_uri();

    return result;
}

static void
ufo_shares_dialog_callback_thread (DialogCallbackThreadArgs *args) {
    common_callback(args->file_info, args->item_name, args->job, FALSE);
    g_free(args->item_name);
    g_free(args);
}

static void
ufo_shares_dialog_callback (GtkDialog *dialog, gint response_id, GtkEntry *entry) {
    if (response_id == GTK_RESPONSE_YES) {
        DialogCallbackThreadArgs *args = malloc(sizeof(DialogCallbackThreadArgs));
        args->file_info = g_object_get_data (G_OBJECT (entry), "NautilusUFOShares::file_info");
        args->job = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (entry), "NautilusUFOShares::job"));
        args->item_name = g_strdup(gtk_editable_get_chars(GTK_EDITABLE (entry), 0, -1 ));

        g_thread_create((GThreadFunc)ufo_shares_dialog_callback_thread, args, FALSE, NULL);
    }
    gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void
ufo_shares_entry_callback (GtkEntry *entry, GtkDialog *dialog) {
    gtk_dialog_response(dialog, GTK_RESPONSE_YES);
}

static void
ufo_shares_sync_callback_thread (GList *data) {
    g_list_foreach(data, (GFunc)copy_to_sync_folder, NULL);
    g_list_foreach(data, (GFunc)g_object_unref, NULL);
}

static void
ufo_shares_create_callback(NautilusMenuItem *item, NautilusFileInfo *file_info) {
    GtkWidget *vbox, *hbox, *entry;
    gchar *title = NULL;
    gchar *desc = NULL;
    gchar *entry_desc = NULL;

    ShareFileJob job = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (item), "NautilusUFOShares::job"));

    switch (job) {
        case SHARING:
            title = _("Find a friend...");
            desc = _("Find a new friend by entering his name, login or email address\nin the following text field.");
            entry_desc = _("Find a friend:");
            break;

        case TAGGING:
            title = _("Create a new tag...");
            desc = _("Create a new tag by entering a name in the \nfollowing text field.");
            entry_desc = _("Create a tag:");
            break;
    }

    GtkWidget *dialog = gtk_dialog_new_with_buttons(title,
                                                    NULL,
                                                    (GtkDialogFlags)
                                                    (/*GTK_DIALOG_MODAL |*/
                                                     GTK_DIALOG_DESTROY_WITH_PARENT),
                                                     GTK_STOCK_OK,
                                                     GTK_RESPONSE_YES,
                                                     GTK_STOCK_CANCEL,
                                                     GTK_RESPONSE_CANCEL, NULL);

    g_signal_connect_swapped (dialog, "delete-event",
                              G_CALLBACK (gtk_widget_destroy),
                              dialog);

    hbox = gtk_hbox_new (FALSE, 0);
    gtk_box_set_spacing (GTK_BOX (hbox), 10);

    entry = gtk_entry_new ();
    g_object_set_data (G_OBJECT (entry), "NautilusUFOShares::job", GUINT_TO_POINTER (job));
    g_object_set_data_full (G_OBJECT (entry), "NautilusUFOShares::file_info", file_info,
                            (GDestroyNotify) g_object_unref);
    g_object_set_data_full (G_OBJECT (dialog), "entry", entry,
                            (GDestroyNotify) g_object_unref);

    g_signal_connect (entry, "activate", G_CALLBACK (ufo_shares_entry_callback), dialog);
    g_signal_connect (dialog, "response", G_CALLBACK (ufo_shares_dialog_callback), entry);

    gtk_box_pack_start (GTK_BOX (hbox), gtk_label_new(entry_desc), FALSE, FALSE, 0);
    gtk_box_pack_end (GTK_BOX (hbox), entry, FALSE, FALSE, 0);
    gtk_widget_show (entry);

    vbox = gtk_dialog_get_content_area(GTK_DIALOG (dialog));
    gtk_box_set_spacing (GTK_BOX (vbox), 10);
    gtk_box_pack_start(GTK_BOX (vbox),
                       gtk_label_new(desc),
                       FALSE, FALSE, 0);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);


    gtk_widget_show_all(dialog);
    gtk_dialog_run(GTK_DIALOG (dialog));
}

static void
ufo_shares_callback_thread (CallbackThreadArgs *args) {
    gchar *item_name;

    gboolean inverse;
    ShareFileJob job;

    job = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (args->item), "NautilusUFOShares::job"));
    inverse = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (args->item), "NautilusUFOShares::inverse"));

    GValue gvalue;
    memset(&gvalue, 0, sizeof(gvalue));
    g_value_init(&gvalue, G_TYPE_STRING);
    g_object_get_property(G_OBJECT(args->item), "label", &gvalue);

    item_name = g_strdup_value_contents(&gvalue);
    /* Remove double quotes... */
    item_name = item_name + 1;
    item_name[strlen(item_name)-1] = '\0';

    common_callback(args->file_info, item_name, job, inverse);
}

static void
ufo_shares_callback(NautilusMenuItem *item, NautilusFileInfo *file_info) {
    CallbackThreadArgs *args = malloc(sizeof(CallbackThreadArgs));
    args->item = item;
    args->file_info = file_info;
    g_thread_create((GThreadFunc)ufo_shares_callback_thread, args, FALSE, NULL);
}

static void
ufo_shares_sync_callback(NautilusMenuItem *item, GList *files) {
    g_thread_create((GThreadFunc)ufo_shares_sync_callback_thread, files, FALSE, NULL);
}

static NautilusMenuItem *
ufo_shares_sync_item_new (GList *files) {
    NautilusMenuItem *ret;
    GList *list;
    gchar *action_name;
    gchar *icon;
    gchar *name;
    gchar *tooltip;

    action_name = g_strdup ("NautilusUFOShares::sync_file");
    name = _("Copy to my synchronized folder");
    tooltip = _("Synchronize a file");
    icon = "/usr/share/nautilus-ufo-shares/icons/ufo-sync.png";

    ret = nautilus_menu_item_new (action_name, name, tooltip, icon);

    list = g_list_copy(files);
    g_object_set_data_full (G_OBJECT (ret), "files", g_object_ref (list),
                            (GDestroyNotify) g_list_free);

    g_signal_connect(ret, "activate", G_CALLBACK(ufo_shares_sync_callback), list);

    return ret;
}

static NautilusMenuItem *
ufo_shares_create_item_new (NautilusFileInfo *file_info, ShareFileJob job) {
    NautilusMenuItem *ret;
    gchar *action_name = NULL;
    gchar *icon = NULL;
    gchar *name = NULL;
    gchar *tooltip = NULL;

    switch (job) {
        case SHARING:
            action_name = g_strdup ("NautilusUFOShares::find_friend");
            name = _("Find a friend...");
            tooltip = _("Find new friends");
            icon = "/usr/share/nautilus-ufo-shares/icons/ufo-friend.png";
            break;

        case TAGGING:
            action_name = g_strdup ("NautilusUFOShares::create_tag");
            name = _("Create a new tag...");
            tooltip = _("Create a new tag");
            icon = "/usr/share/nautilus-ufo-shares/icons/ufo-newtag.png";
            break;
    }

    ret = nautilus_menu_item_new (action_name, name, tooltip, icon);

    g_object_set_data (G_OBJECT (ret), "NautilusUFOShares::job", GUINT_TO_POINTER (job));
    g_object_set_data_full (G_OBJECT (ret), "file-info", g_object_ref (file_info),
                            (GDestroyNotify) g_object_unref);

    g_signal_connect(ret, "activate", G_CALLBACK(ufo_shares_create_callback), file_info);

    return ret;
}

static NautilusMenuItem *
ufo_shares_menu_item_new (NautilusFileInfo *file_info,
                          ShareFileJob      menu_item_job,
                          GList            *existings,
                          gboolean          inverse,
                          gboolean          disabled,
                          gboolean          recache_uri)
{
    NautilusMenuItem *ret;
    gchar *action_name = NULL;
    gchar *name = NULL;
    gchar *icon = NULL;
    gchar *tooltip = NULL;

    GList *sub_menu_items = NULL;
    NautilusMenuItem *extended_seletion_item = NULL;

    switch (menu_item_job) {
        case SHARING:
            if (!inverse) {
                name = _("Share with");
                tooltip = _("Share with a friend");
                action_name = g_strdup ("NautilusUFOShares::share");
                icon = "/usr/share/nautilus-ufo-shares/icons/ufo-share.png";

                if (!disabled) {
                    if (recache_uri)
                        parts_dirents = item_list_from_dirents(sub_menu_items,
                                                               sharesviewpath,
                                                               existings);
                    sub_menu_items = parts_dirents;
                    extended_seletion_item = ufo_shares_create_item_new(file_info, menu_item_job);
                }

            } else {
                name = _("Stop sharing with");
                tooltip = _("Remove a friend from the share");
                action_name = g_strdup ("NautilusUFOShares::unshare");
                icon = "/usr/share/nautilus-ufo-shares/icons/ufo-unshare.png";

                sub_menu_items = existings;
            }
            break;

        case TAGGING:
            if (!inverse) {
                name = _("Tag as");
                tooltip = _("Add a tag to file");
                action_name = g_strdup ("NautilusUFOShares::tag");
                icon = "/usr/share/nautilus-ufo-shares/icons/ufo-tag.png";

                if (!disabled) {
                    if (recache_uri)
                        tags_dirents = item_list_from_dirents(sub_menu_items,
                                                              tagsviewpath,
                                                                 existings);
                    sub_menu_items = tags_dirents;
                    extended_seletion_item = ufo_shares_create_item_new(file_info, menu_item_job);
                }

            } else {
                name = _("Untag as");
                tooltip = _("Remove a tag from file");
                action_name = g_strdup ("NautilusUFOShares::untag");
                icon = "/usr/share/nautilus-ufo-shares/icons/ufo-untag.png";

                sub_menu_items = existings;
            }
            break;
    }

    ret = nautilus_menu_item_new (action_name, name, tooltip, icon);

    if (sub_menu_items != NULL || extended_seletion_item != NULL) {
        NautilusMenu     *submenu;
        NautilusMenuItem *item;
        gchar            *item_action_name;
        gchar            *item_name;

        submenu = nautilus_menu_new();
        nautilus_menu_item_set_submenu(ret, submenu);

        if (extended_seletion_item != NULL)
            nautilus_menu_append_item(submenu, extended_seletion_item);

        while (sub_menu_items != NULL) {
            item_name = g_list_nth_data (sub_menu_items, 0);
            item_action_name = g_strdup_printf("%s_with_%s", action_name, item_name);

            item = nautilus_menu_item_new(item_action_name, item_name, item_name, NULL);

            g_object_set_data (G_OBJECT (item), "NautilusUFOShares::job", GUINT_TO_POINTER (menu_item_job));
            g_object_set_data (G_OBJECT (item), "NautilusUFOShares::inverse", GUINT_TO_POINTER (inverse));

            g_object_set_data_full (G_OBJECT (item), "file-info", g_object_ref (file_info),
                                    (GDestroyNotify) g_object_unref);

            g_signal_connect(item, "activate", G_CALLBACK(ufo_shares_callback), file_info);
            nautilus_menu_append_item(submenu, item);

            g_free(item_action_name);
            sub_menu_items = sub_menu_items->next;
        }

    } else {
        disable_menu_item (ret);
    }

    g_free (action_name);

    return ret;
}

static void
try_retrieve_username () {
    gchar *user = extended_attribute_to_string (overlaypath, "tsumufs.dbname");

    if (user != NULL) {
        userdirpath = g_strdup_printf("%s/%s", overlaypath, user);
    }
}

GList *
nautilus_ufo_shares_get_file_items (NautilusMenuProvider *provider,
                                    GtkWidget            *window,
                                    GList                *files)
{
    GList *items = NULL;
    gchar *activation_uri = NULL;
    gchar *uri = NULL;
    NautilusMenuItem *item;
    NautilusFileInfo *file_info;

    gboolean disabled = FALSE;
    gboolean recache_uri = FALSE;

    file_info = g_list_nth_data (files, 0);

    activation_uri = nautilus_file_info_get_activation_uri (file_info);

    if (activation_uri == NULL)
        return items;

    uri = g_filename_from_uri (activation_uri, NULL, NULL);

    recache_uri = (cached_uri == NULL || strcmp(cached_uri, uri) != 0);
    if (recache_uri) {
        if (cached_uri != NULL)
            invalidate_cached_uri ();

        cached_uri = g_strdup (uri);
        share_file_info = get_share_file_info (uri);
    }

    if (share_file_info == NONE_FILE) {
        if (overlaypath != NULL && userdirpath == NULL)
            try_retrieve_username();

        if (userdirpath != NULL) {
            item  = ufo_shares_sync_item_new (files);
            items = g_list_append (items, item);

            int i;
            for (i = 0; i < g_list_length(files); i++) {
                if (nautilus_file_info_is_directory(g_list_nth_data (files, i))) {
                    disable_menu_item (item);
                    break;
                }
            }
        }
        return items;
    }

    /* All menu items are disabled when multiple selection,
     * or when the selected uri is a directory */
    if (g_list_length(files) > 1 ||
        share_file_info == OTHER_FILE ||
        nautilus_file_info_is_directory(file_info)) {
        disabled = TRUE;

    } else {
        if (recache_uri) {
            tags = extended_attribute_to_list(tags, uri, "tsumufs.staredbytag.tags");
        }
        if (recache_uri) {
            parts = extended_attribute_to_list(parts, uri, "tsumufs.myshares.participants");
        }
    }

    if (tagsviewpath == NULL)
        tagsviewpath = extended_attribute_to_string(uri, "tsumufs.staredbytag.path");
    if (sharesviewpath == NULL)
        sharesviewpath = extended_attribute_to_string(uri, "tsumufs.myshares.path");

    if (tagsviewpath == NULL || sharesviewpath == NULL)
        return items;

    /* Share file action */
    item  = ufo_shares_menu_item_new (file_info, SHARING, parts, FALSE, disabled, recache_uri);
    items = g_list_append (items, item);

    /* Unshare file action, disabled when file not shared */
    item = ufo_shares_menu_item_new (file_info, SHARING, parts, TRUE, disabled, recache_uri);
    items = g_list_append (items, item);

    /* Tag file action */
    item  = ufo_shares_menu_item_new (file_info, TAGGING, tags, FALSE, disabled, recache_uri);
    items = g_list_append (items, item);

    /* Tag file action, disabled when file not tagged */
    item = ufo_shares_menu_item_new (file_info, TAGGING, tags, TRUE, disabled, recache_uri);
    items = g_list_append (items, item);

    g_free (uri);
    g_free (activation_uri);

    return items;
}

static NautilusOperationResult
nautilus_ufo_shares_update_file_info (NautilusInfoProvider     *provider,
                                      NautilusFileInfo         *file,
                                      GClosure                 *update_complete,
                                      NautilusOperationHandle **handle) {
    gchar *activation_uri, *uri;

    activation_uri = nautilus_file_info_get_uri (file);
    uri = g_filename_from_uri(activation_uri, NULL, NULL);
    g_free(activation_uri);

    if (uri == NULL || overlaypath == NULL) {
        return NAUTILUS_OPERATION_COMPLETE;
    }

    NautilusFileInfoIface *file_iface = NAUTILUS_FILE_INFO_GET_IFACE(file);

	int size;
	char value[1];

	size = getxattr(uri, "tsumufs.dirty", value, 1);
	if (size < 0)
		return NAUTILUS_OPERATION_COMPLETE;

	if (atoi(value) == 1)
		file_iface->add_emblem(file, "important");
	else
		file_iface->add_emblem(file, "default");

    return NAUTILUS_OPERATION_COMPLETE;
}

static GList *
nautilus_ufo_shares_get_background_items (NautilusMenuProvider *provider,
                                          GtkWidget             *window,
                                          NautilusFileInfo      *file_info)
{
    return NULL;
}

static void
nautilus_ufo_shares_cancel_update(NautilusInfoProvider    *provider,
                                  NautilusOperationHandle *handle)
{
  return;
}

static void
nautilus_ufo_shares_menu_provider_iface_init (NautilusMenuProviderIface *iface)
{
    iface->get_background_items = nautilus_ufo_shares_get_background_items;
    iface->get_file_items = nautilus_ufo_shares_get_file_items;
}

static void
nautilus_ufo_shares_info_provider_iface_init (NautilusInfoProviderIface *iface) {
    iface->update_file_info = nautilus_ufo_shares_update_file_info;
    iface->cancel_update = nautilus_ufo_shares_cancel_update;
}

static void 
nautilus_ufo_shares_instance_init (NautilusUFOShares *cvs) {
}

static void
nautilus_ufo_shares_class_init (NautilusUFOSharesClass *class) {
    GKeyFile *tsumufs_config = g_key_file_new();

    int ret = g_key_file_load_from_file(tsumufs_config, "/etc/tsumufs/tsumufs.conf",
                                         G_KEY_FILE_NONE, NULL);

    if (ret) {
        overlaypath = g_key_file_get_string(tsumufs_config,
                                            g_key_file_get_start_group(tsumufs_config),
                                            "overlay", NULL);

        if (overlaypath != NULL)
            try_retrieve_username();
    }
    g_key_file_free (tsumufs_config);

    notify_init("nautilus-ufo-shares");
}

static void
nautilus_ufo_shares_class_finalize (NautilusUFOSharesClass *class) {

}

GType
nautilus_ufo_shares_get_type (void)
{
    return ufo_shares_type;
}

void
nautilus_ufo_shares_register_type (GTypeModule *module)
{
    static const GTypeInfo info = {
        sizeof (NautilusUFOSharesClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) nautilus_ufo_shares_class_init,
        (GClassFinalizeFunc) nautilus_ufo_shares_class_finalize,
        NULL,
        sizeof (NautilusUFOShares),
        0,
        (GInstanceInitFunc) nautilus_ufo_shares_instance_init,
    };

    static const GInterfaceInfo menu_provider_iface_info = {
        (GInterfaceInitFunc) nautilus_ufo_shares_menu_provider_iface_init,
        NULL,
        NULL
    };

    static const GInterfaceInfo info_provider_iface_info = {
      (GInterfaceInitFunc) nautilus_ufo_shares_info_provider_iface_init,
      NULL,
      NULL
    };

    ufo_shares_type = g_type_module_register_type (module,
                                                   G_TYPE_OBJECT,
                                                   "NautilusUFOShares",
                                                   &info, 0);

    g_type_module_add_interface (module,
                                 ufo_shares_type,
                                 NAUTILUS_TYPE_MENU_PROVIDER,
                                 &menu_provider_iface_info);

    g_type_module_add_interface (module,
                                 ufo_shares_type,
                                 NAUTILUS_TYPE_INFO_PROVIDER,
                                 &info_provider_iface_info);
}
